FROM python:3.8

RUN apt update

RUN mkdir /srv/project
WORKDIR /srv/project

COPY ./myproject ./src
COPY ./myproject/requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

ENV TZ Europe/Kiev

CMD ["bash"]
